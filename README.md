## rdl (melodic) - 3.2.0-1

The packages in the `rdl` repository were released into the `melodic` distro by running `/usr/local/bin/bloom-release --rosdistro melodic --track melodic rdl --edit` on `Sun, 12 May 2019 12:20:58 -0000`

These packages were released:
- `rdl`
- `rdl_benchmark`
- `rdl_cmake`
- `rdl_dynamics`
- `rdl_msgs`
- `rdl_ros_tools`
- `rdl_urdfreader`

Version of package(s) in repository `rdl`:

- upstream repository: https://gitlab.com/jlack/rdl.git
- release repository: unknown
- rosdistro version: `null`
- old version: `null`
- new version: `3.2.0-1`

Versions of tools used:

- bloom version: `0.8.0`
- catkin_pkg version: `0.4.12`
- rosdep version: `0.15.1`
- rosdistro version: `0.7.4`
- vcstools version: `0.1.40`


## rdl (kinetic) - 1.1.0-0

The packages in the `rdl` repository were released into the `kinetic` distro by running `/usr/bin/bloom-release --rosdistro kinetic --track kinetic rdl --edit` on `Mon, 11 Mar 2019 17:37:27 -0000`

These packages were released:
- `rdl`
- `rdl_benchmark`
- `rdl_cmake`
- `rdl_dynamics`
- `rdl_msgs`
- `rdl_ros_tools`
- `rdl_urdfreader`

Version of package(s) in repository `rdl`:

- upstream repository: https://gitlab.com/jlack/rdl.git
- release repository: https://gitlab.com/jlack/rdl_release.git
- rosdistro version: `1.0.0-0`
- old version: `1.0.0-0`
- new version: `1.1.0-0`

Versions of tools used:

- bloom version: `0.7.2`
- catkin_pkg version: `0.4.10`
- rosdep version: `0.15.1`
- rosdistro version: `0.7.2`
- vcstools version: `0.1.40`


## rdl (kinetic) - 1.0.0-0

The packages in the `rdl` repository were released into the `kinetic` distro by running `/usr/bin/bloom-release --rosdistro kinetic --track kinetic rdl --edit` on `Sun, 28 Oct 2018 21:32:29 -0000`

These packages were released:
- `rdl`
- `rdl_benchmark`
- `rdl_cmake`
- `rdl_dynamics`
- `rdl_urdfreader`

Version of package(s) in repository `rdl`:

- upstream repository: https://gitlab.com/jlack/rdl.git
- release repository: https://gitlab.com/jlack/rdl_release.git
- rosdistro version: `0.10.6-1`
- old version: `0.10.6-1`
- new version: `1.0.0-0`

Versions of tools used:

- bloom version: `0.6.7`
- catkin_pkg version: `0.4.9`
- rosdep version: `0.12.2`
- rosdistro version: `0.6.9`
- vcstools version: `0.1.40`


## rdl (kinetic) - 0.10.6-1

The packages in the `rdl` repository were released into the `kinetic` distro by running `/usr/bin/bloom-release --rosdistro kinetic --track kinetic --edit rdl` on `Mon, 18 Jun 2018 12:07:56 -0000`

These packages were released:
- `rdl`
- `rdl_benchmark`
- `rdl_cmake`
- `rdl_dynamics`
- `rdl_urdfreader`

Version of package(s) in repository `rdl`:

- upstream repository: https://gitlab.com/jlack/rdl.git
- release repository: https://gitlab.com/jlack/rdl_release.git
- rosdistro version: `0.9.5-0`
- old version: `0.10.6-0`
- new version: `0.10.6-1`

Versions of tools used:

- bloom version: `0.6.4`
- catkin_pkg version: `0.4.2`
- rosdep version: `0.12.2`
- rosdistro version: `0.6.8`
- vcstools version: `0.1.40`


## rdl (kinetic) - 0.10.6-0

The packages in the `rdl` repository were released into the `kinetic` distro by running `/usr/bin/bloom-release --rosdistro kinetic --track kinetic --edit rdl` on `Mon, 18 Jun 2018 12:02:52 -0000`

These packages were released:
- `rdl`
- `rdl_benchmark`
- `rdl_cmake`
- `rdl_dynamics`
- `rdl_urdfreader`

Version of package(s) in repository `rdl`:

- upstream repository: https://gitlab.com/jlack/rdl.git
- release repository: https://gitlab.com/jlack/rdl_release.git
- rosdistro version: `0.9.5-0`
- old version: `0.9.5-0`
- new version: `0.10.6-0`

Versions of tools used:

- bloom version: `0.6.4`
- catkin_pkg version: `0.4.2`
- rosdep version: `0.12.2`
- rosdistro version: `0.6.8`
- vcstools version: `0.1.40`


## rdl (kinetic) - 0.9.5-0

The packages in the `rdl` repository were released into the `kinetic` distro by running `/usr/bin/bloom-release rdl --track kinetic --rosdistro kinetic --edit` on `Sat, 16 Dec 2017 16:33:04 -0000`

These packages were released:
- `rdl`
- `rdl_benchmark`
- `rdl_cmake`
- `rdl_dynamics`
- `rdl_urdfreader`

Version of package(s) in repository `rdl`:

- upstream repository: https://gitlab.com/jlack/rdl.git
- release repository: https://gitlab.com/jlack/rdl_release.git
- rosdistro version: `0.9.4-0`
- old version: `0.9.4-0`
- new version: `0.9.5-0`

Versions of tools used:

- bloom version: `0.6.1`
- catkin_pkg version: `0.3.9`
- rosdep version: `0.11.8`
- rosdistro version: `0.6.2`
- vcstools version: `0.1.39`


## rdl (kinetic) - 0.9.4-0

The packages in the `rdl` repository were released into the `kinetic` distro by running `/usr/bin/bloom-release rdl --track kinetic --rosdistro kinetic --edit` on `Thu, 14 Dec 2017 11:44:57 -0000`

These packages were released:
- `rdl`
- `rdl_benchmark`
- `rdl_cmake`
- `rdl_dynamics`
- `rdl_urdfreader`

Version of package(s) in repository `rdl`:

- upstream repository: https://gitlab.com/jlack/rdl.git
- release repository: https://gitlab.com/jlack/rdl_release.git
- rosdistro version: `0.9.3-0`
- old version: `0.9.3-0`
- new version: `0.9.4-0`

Versions of tools used:

- bloom version: `0.6.1`
- catkin_pkg version: `0.3.9`
- rosdep version: `0.11.8`
- rosdistro version: `0.6.2`
- vcstools version: `0.1.39`


## rdl (kinetic) - 0.9.3-0

The packages in the `rdl` repository were released into the `kinetic` distro by running `/usr/bin/bloom-release --rosdistro kinetic --track kinetic rdl --edit` on `Tue, 12 Dec 2017 13:47:46 -0000`

These packages were released:
- `rdl`
- `rdl_benchmark`
- `rdl_cmake`
- `rdl_dynamics`
- `rdl_urdfreader`

Version of package(s) in repository `rdl`:

- upstream repository: https://gitlab.com/jlack/rdl.git
- release repository: https://gitlab.com/jlack/rdl_release.git
- rosdistro version: `null`
- old version: `0.9.1-1`
- new version: `0.9.3-0`

Versions of tools used:

- bloom version: `0.6.1`
- catkin_pkg version: `0.3.9`
- rosdep version: `0.11.8`
- rosdistro version: `0.6.2`
- vcstools version: `0.1.39`


## rdl (kinetic) - 0.9.1-1

The packages in the `rdl` repository were released into the `kinetic` distro by running `/usr/bin/bloom-release --rosdistro kinetic --track kinetic rdl --edit` on `Sat, 09 Dec 2017 00:51:52 -0000`

These packages were released:
- `rdl`
- `rdl_benchmark`
- `rdl_cmake`
- `rdl_dynamics`
- `rdl_urdfreader`

Version of package(s) in repository `rdl`:

- upstream repository: https://gitlab.com/jlack/rdl.git
- release repository: unknown
- rosdistro version: `null`
- old version: `0.9.1-0`
- new version: `0.9.1-1`

Versions of tools used:

- bloom version: `0.6.1`
- catkin_pkg version: `0.3.9`
- rosdep version: `0.11.8`
- rosdistro version: `0.6.2`
- vcstools version: `0.1.39`


## rdl (kinetic) - 0.9.1-0

The packages in the `rdl` repository were released into the `kinetic` distro by running `/usr/bin/bloom-release --rosdistro kinetic --track kinetic rdl --edit` on `Sat, 09 Dec 2017 00:43:51 -0000`

These packages were released:
- `rdl`
- `rdl_benchmark`
- `rdl_cmake`
- `rdl_dynamics`
- `rdl_urdfreader`

Version of package(s) in repository `rdl`:

- upstream repository: https://gitlab.com/jlack/rdl.git
- release repository: unknown
- rosdistro version: `null`
- old version: `null`
- new version: `0.9.1-0`

Versions of tools used:

- bloom version: `0.6.1`
- catkin_pkg version: `0.3.9`
- rosdep version: `0.11.8`
- rosdistro version: `0.6.2`
- vcstools version: `0.1.39`


